Using Logistic Regression Model to predict Popularity of Music Record

Taking an analytics approach, we aim to use information about a song's properties to predict its popularity. 
The dataset songs.csv consists of all songs which made it to the Top 10 of the Billboard Hot 100 Chart from 1990-2010 
plus a sample of additional songs that didn't make the Top 10. 

# 1. Load the data into R
song <- read.csv("songs.csv")

# 2. Check the data structure to know how many observation and the variables
str(song)

# 3. Split the dataset into trainset and testset
SongsTrain <- subset(song, year <= 2009)
SongsTest <- subset(song, year == 2010)

#In this problem, our outcome variable is "Top10" - we are trying to predict whether or 
#not a song will make it to the Top 10 of the Billboard Hot 100 Chart.

#3. We will only use the variables in our dataset that describe the numerical attributes of 
#the song in our logistic regression model
#variables "year", "songtitle", "artistname", "songID" or "artistID" will not be used 
nonvars = c("year", "songtitle", "artistname", "songID", "artistID")

#To remove these variables from your training and testing sets

SongsTrain = SongsTrain[ , !(names(SongsTrain) %in% nonvars) ]
SongsTest = SongsTest[ , !(names(SongsTest) %in% nonvars) ]

# 4 Use the glm function to build a logistic regression model to predict Top10 using all of 
#the other variables as the independent variables

SongsLog1 = glm(Top10 ~ ., data=SongsTrain, family=binomial)

# identify Akaike Information Criterion (AIC). Lower AIC the better the model.
summary(SonsLog1)

# 5. model seems to indicate that these confidence variables are significant 
#(rather than the variables timesignature, key and tempo themselves). 
#Some of the variables are highly correlated. For instance, loudness and energy. 
# We will omit of the two to build another model. Omit loudness in the second model.s

SongsLog2 = glm(Top10 ~ . - loudness, data=SongsTrain, family=binomial)

#6. Create third model with loudness instead of energy

SongsLog3 = glm(Top10 ~ . - energy, data=SongsTrain, family=binomial)

# 7. Make predictions on the test set using Model 3
predictTest <- predict(SongsLog3, newdata=SongsTest, type="response")

# 8. What is the accuracy of Model 3 on the test set, using a threshold of 0.45?
table(SongsTest$Top10, predictTest >= 0.45)
  #Overall accuracy = (True negative + True positive)/number of observation

# 9. What would the accuracy of the baseline model be on the test set

table(SongsTest$Top10)

#False/(True + False)

# 10. How many songs does Model 3 correctly predict as Top 10 hits in 2010 using a 
#threshold of 0.45?
   table(SongsTest$Top10, predictTest >= 0.45)
   # True Positive

# 10. How many non-hit songs does Model 3 predict will be Top 10 hits, 
#using a threshold of 0.45?
   #False_Positive

# 11. What is the sensitivity of Model 3 on the test set, using a threshold of 0.45?
    #Sensitivity = True_Positive/(True_Positive + False_Positive)

#12. What is the specificity of Model 3 on the test set, using a threshold of 0.45?
   #Specificity = False_negative/(False_negative + True_negative)



